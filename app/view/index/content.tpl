<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
  <title>做人的心计（你一定要看） | 天瓜网</title> 
  <meta name="keywords" content="做人,心计,一定,要看" /> 
  <meta name="description" content="做人的心计，你一定要看，未必人人都懂：
一、聪明外露，不如智慧深藏
二、把握做人的尺度
三、小心驶得万年船
四、树活一张皮，人活一张脸
五、祸从口出福从口入
六、做人要能方能圆
七、礼多人不怪
八、八面玲珑路路通
" /> 
  <meta name="author" content="天瓜网|天瓜|每日一美文,胜过十本书" /> 
  <link rel="stylesheet" type="text/css" href="/static/index/css/base.min.css" /> 
  <link rel="stylesheet" type="text/css" href="/static/index/css/com.min.css" />  
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
 </head>
 <body> 
  <!--页面开始--> 
  <div class="main-wraper"> 
   <!--头部开始--> {include file='index/lib/header.tpl'} 
   <!--头部结束--> 
   <!--中部--> 
   <div class="main no-log-main"> 
    <!--中部右边开始--> 
    <div class="g-r-n"> 
     <div class="sub-bar"> 
      <p class="hd get_lianbo_hot">阅读排行</p> 
      <dl class="sub-list" id="getTopList">
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3256">谁能坚持超过50秒，我请他吃饭!</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3257">你看了必转走的图片</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3275">天瓜独家首发《2048恋爱脱光版》，不玩你就OUT了</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3258">5分钟看完，50年领悟</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3259">看完泪奔，你有资格抱怨吗</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3283">这篇日志和视频曾经感动了上百万人，今天是个特殊的日子，好久没流泪了</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3276">一份没人敢看的中国地图</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3274">你看了必会有接吻的冲动</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3265">一段视频就是一辈子</a>
       </dt>
       <dt class="sub-cp dragon">
        <a target="_self" href="http://www.daygua.com/3273">价值5千万的1堂课（不看白不看）</a>
       </dt>
      </dl> 
      <p class="ft"><a href="http://www.daygua.com/top">查看更多</a></p> 
     </div> 
     <div class="sub-bar"> 
      <p class="hd get_lianbo_hot">评论排行</p> 
      <dl class="sub-list"> 
       <!-- 评论排行 start --> 
       <div class="ds-top-threads" data-range="monthly" data-num-items="10" id="ds-top-threads">
        <li><a target="_blank" href="http://www.daygua.com/3256" title="谁能坚持超过50秒，我请他吃饭!">谁能坚持超过50秒，我请他吃饭!</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3265" title="一段视频就是一辈子">一段视频就是一辈子</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3267" title="人间小精灵，你一定会汗颜！">人间小精灵，你一定会汗颜！</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3257" title="你看了必转走的图片">你看了必转走的图片</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3273" title="价值5千万的1堂课（不看白不看）">价值5千万的1堂课（不看白不看）</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3259" title="看完泪奔，你有资格抱怨吗">看完泪奔，你有资格抱怨吗</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3262" title="你穷是因为你没有野心">你穷是因为你没有野心</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3270" title="多年积攒的正能量，吐血力荐">多年积攒的正能量，吐血力荐</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3258" title="5分钟看完，50年领悟">5分钟看完，50年领悟</a></li>
        <li><a target="_blank" href="http://www.daygua.com/3261" title="人生的70%与30%">人生的70%与30%</a></li>
       </div> 
       <!-- 评论排行 end --> 
      </dl> 
     </div> 
    </div> 
    <!--中部右边结束--> 
    <!--中部左边开始--> 
    <div class="main-content"> 
     <div class="article  song"> 
      <ul class="bd article-list doings_list" data-id="" id="doings-list"> 
       <li class="entry"> 
        <div class="art-desc"> 
         <h3 class="art-t"> <b title="做人的心计（你一定要看）" style="font-size:24px;">做人的心计（你一定要看）</b> </h3> 
         <p> <span class="art-main"> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#E53333;">做人的心计，你一定要看，未必人人都懂：</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;">一、聪明外露，不如智慧深藏</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 二、把握做人的尺度</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 三、小心驶得万年船</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 四、树活一张皮，人活一张脸</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 五、祸从口出福从口入</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 六、做人要能方能圆</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 七、礼多人不怪</span><br /> <span style="font-family:'Microsoft YaHei';font-size:16px;line-height:2;color:#9933E5;"> 八、八面玲珑路路通</span><br /> <img src="./content_files/31398779771539.jpg" width="625" height="469" alt="" data-pinit="registered" /><img src="./content_files/91398779780158.jpg" width="625" height="469" alt="" data-pinit="registered" /><img src="./content_files/31398779789677.jpg" alt="" data-pinit="registered" /><img src="./content_files/41398779797451.jpg" width="625" height="470" alt="" data-pinit="registered" /><img src="./content_files/31398779807742.jpg" width="625" height="453" alt="" data-pinit="registered" /><img src="./content_files/41398779815850.jpg" width="625" height="472" alt="" data-pinit="registered" /><img src="./content_files/91398779826835.jpg" width="625" height="473" alt="" data-pinit="registered" /><img src="./content_files/71398779833582.jpg" width="625" height="472" alt="" data-pinit="registered" /><br /> <br /> <br /> </span> </p> 
        </div> 
        <div class="art_ft"> 
         <div class="v art-pub"> 
          <!--分享工具开始--> 
          <div class="bdsharebuttonbox bdshare-button-style2-16" data-bd-bind="1400169596677">
           <a href="http://www.daygua.com/random#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间">QQ空间</a>
           <a href="http://www.daygua.com/random#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博">新浪微博</a>
           <a href="http://www.daygua.com/random#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博">腾讯微博</a>
           <a href="http://www.daygua.com/random#" class="bds_renren" data-cmd="renren" title="分享到人人网">人人</a>
           <a href="http://www.daygua.com/random#" class="bds_tqf" data-cmd="tqf" title="分享到腾讯朋友">朋友网</a>
           <a href="http://www.daygua.com/random#" class="bds_more" data-cmd="more">分享</a>
          </div> 
          <!--分享工具结束--> 
         </div> 
         <div class="v art-info"> 
          <b style="font-size:16px;">2014-04-30</b> 
          <input value="2014-04-30" name="article_showTime" type="hidden" id="article_showTime" /> 
         </div> 
        </div> </li> 
      </ul> 
     </div> 
     <!--上下篇开始--> 
     <div class="index-page-item"> 
      <div class="lastArticle" id="article_lastArticle"> 
       <a title="价值5千万的1堂课（不看白不看）" onfocus="this.blur()" href="http://www.daygua.com/3273">上篇：价值5千万的1堂课（不看白不看）</a> 
      </div> 
      <div class="nextArticle" id="article_nextArticle"> 
       <a title="幸福额度" onfocus="this.blur()" href="http://www.daygua.com/3271">下篇：幸福额度</a> 
      </div> 
     </div> 
     <!--上下篇结束--> 
     <br />
     <br /> 
     <!-- duoshuoStart --> 
     <div class="ds-thread" data-thread-key="3272" data-title="做人的心计（你一定要看）" data-url="http://www.daygua.com/3272" id="ds-thread">
      <div id="ds-reset">
       <div class="ds-meta" style="display: none;">
        <a href="javascript:void(0)" class="ds-like-thread-button ds-rounded"><span class="ds-icon ds-icon-heart"></span> <span class="ds-thread-like-text">喜欢</span><span class="ds-thread-cancel-like">取消喜欢</span></a>
        <span class="ds-like-panel"></span>
       </div>
       <div class="ds-comments-info">
        <div class="ds-sort">
         <a class="ds-order-desc ds-current">最新</a>
         <a class="ds-order-asc">最早</a>
         <a class="ds-order-hot">最热</a>
        </div>
        <ul class="ds-comments-tabs">
         <li class="ds-tab"><a class="ds-comments-tab-duoshuo ds-current" href="javascript:void(0);"><span class="ds-highlight">7</span>条评论</a></li>
        </ul>
       </div>
       <ul class="ds-comments">
        <li class="ds-post" data-post-id="1407003248623092454">
         <div class="ds-post-self" data-post-id="1407003248623092454" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="不伤风" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">不伤风</span>
           </div>
           <p>好美的图 <img src="./content_files/vw_org.gif" alt="[威武]" title="[威武]" class="ds-smiley" /> </p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-30T16:55:06+08:00" title="2014年4月30日 下午4:55:06" style="display: none;">4月30日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092453">
         <div class="ds-post-self" data-post-id="1407003248623092453" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="O(∩_∩)O~" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">O(∩_∩)O~</span>
           </div>
           <p>厉害(⊙o⊙)哦</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-30T09:39:41+08:00" title="2014年4月30日 上午9:39:41" style="display: none;">4月30日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092452">
         <div class="ds-post-self" data-post-id="1407003248623092452" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="舞儿__" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">舞儿__</span>
           </div>
           <p>精粹精辟</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-29T22:51:29+08:00" title="2014年4月29日 下午10:51:29" style="display: none;">4月29日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092451">
         <div class="ds-post-self" data-post-id="1407003248623092451" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="黄山脚下" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">黄山脚下</span>
           </div>
           <p>太好了。学习了！谢谢。</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-29T22:50:37+08:00" title="2014年4月29日 下午10:50:37" style="display: none;">4月29日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092450">
         <div class="ds-post-self" data-post-id="1407003248623092450" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="烛光" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">烛光</span>
           </div>
           <p>深有感触，做人也要讲究学问</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-29T22:50:01+08:00" title="2014年4月29日 下午10:50:01" style="display: none;">4月29日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092449">
         <div class="ds-post-self" data-post-id="1407003248623092449" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="来自星星的你" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">来自星星的你</span>
           </div>
           <p>给我启发不少，谢谢天瓜</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-29T22:49:46+08:00" title="2014年4月29日 下午10:49:46" style="display: none;">4月29日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
        <li class="ds-post" data-post-id="1407003248623092448">
         <div class="ds-post-self" data-post-id="1407003248623092448" data-thread-id="1407003248623091824" data-root-id="0" data-source="duoshuo">
          <div class="ds-avatar">
           <img src="./content_files/87674.jpg" alt="肥妈猪一" />
          </div>
          <div class="ds-comment-body">
           <div class="ds-comment-header">
            <span class="ds-user-name" data-qqt-account="">肥妈猪一</span>
           </div>
           <p>说的真经典，thanks</p>
           <div class="ds-comment-footer ds-comment-actions">
            <span class="ds-time" datetime="2014-04-29T22:49:24+08:00" title="2014年4月29日 下午10:49:24" style="display: none;">4月29日</span>
            <a class="ds-post-reply" href="javascript:void(0);"><span class="ds-icon ds-icon-reply"></span>回复</a>
            <a class="ds-post-likes" href="javascript:void(0);"><span class="ds-icon ds-icon-like"></span>顶</a>
            <a class="ds-post-repost" href="javascript:void(0);"><span class="ds-icon ds-icon-share"></span>转发</a>
            <a class="ds-post-report" href="javascript:void(0);"><span class="ds-icon ds-icon-report"></span>举报</a>
           </div>
          </div>
         </div></li>
       </ul>
       <div class="ds-paginator" style="display: none;">
        <div class="ds-border"></div>
        <a data-page="1" href="javascript:void(0);" class="ds-current">1</a>
       </div>
       <a name="respond"></a>
       <div class="ds-login-buttons">
        <p>社交帐号登录:</p>
        <div class="ds-social-links">
         <ul class="ds-service-list">
          <li><a href="http://daygua.duoshuo.com/login/weibo/" rel="nofollow" class="ds-service-link ds-weibo">微博</a></li>
          <li><a href="http://daygua.duoshuo.com/login/qq/" rel="nofollow" class="ds-service-link ds-qq">QQ</a></li>
          <li><a href="http://daygua.duoshuo.com/login/renren/" rel="nofollow" class="ds-service-link ds-renren">人人</a></li>
          <li><a href="http://daygua.duoshuo.com/login/douban/" rel="nofollow" class="ds-service-link ds-douban">豆瓣</a></li>
          <li><a class="ds-more-services" href="javascript:void(0)">更多&raquo;</a></li>
         </ul>
         <ul class="ds-service-list ds-additional-services">
          <li><a href="http://daygua.duoshuo.com/login/kaixin/" rel="nofollow" class="ds-service-link ds-kaixin">开心</a></li>
          <li><a href="http://daygua.duoshuo.com/login/netease/" rel="nofollow" class="ds-service-link ds-netease">网易</a></li>
          <li><a href="http://daygua.duoshuo.com/login/sohu/" rel="nofollow" class="ds-service-link ds-sohu">搜狐</a></li>
          <li><a href="http://daygua.duoshuo.com/login/baidu/" rel="nofollow" class="ds-service-link ds-baidu">百度</a></li>
          <li><a href="http://daygua.duoshuo.com/login/google/" rel="nofollow" class="ds-service-link ds-google">谷歌</a></li>
         </ul>
        </div>
       </div>
       <div class="ds-replybox">
        <a class="ds-avatar" href="javascript:void(0);" onclick="return false"><img src="./content_files/87674.jpg" alt="" /></a>
        <form method="post">
         <input type="hidden" name="thread_id" value="1407003248623091824" /> 
         <input type="hidden" name="parent_id" value="" /> 
         <input type="hidden" name="nonce" value="5374e3e63c1ba" />
         <div class="ds-textarea-wrapper ds-rounded-top">
          <textarea name="message" title="Ctrl+Enter快捷提交" placeholder="说点什么吧…"></textarea>
          <pre class="ds-hidden-text"></pre>
         </div>
         <div class="ds-post-toolbar">
          <div class="ds-post-options ds-gradient-bg">
           <span class="ds-sync"></span>
          </div>
          <button class="ds-post-button" type="submit">免登录评论</button>
          <div class="ds-toolbar-buttons">
           <a class="ds-toolbar-button ds-add-emote" title="插入表情"></a>
          </div>
         </div>
        </form>
       </div>
       <p class="ds-powered-by"></p>
      </div>
     </div> 
     <!-- duoshuoEnd --> 
    </div> 
    <!--中部左边结束--> 
   </div> 
   <!--中部结束--> 
   <!--底部结束--> {include file='index/lib/footer.tpl'} 
   <!--底部结束--> 
  </div> 
  <!--页面结束--> 
 </body>
</html>