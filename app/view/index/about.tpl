<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
  <title>关于天瓜 | 天瓜</title> 
  <meta name="keywords" content="天瓜网,天瓜,天瓜美文,智慧,青春,励志,正能量,美文,经典语录,人生感悟,唯美文章,励志文章,瓜,tiangua,daygua,daygua.com" /> 
  <meta name="description" content="天瓜网是每日精选出一篇美文的最受欢迎的传播正能量的清新网站，文章句句经典感动，是每日必读必上的网站，每日一美文，胜过十本书。" /> 
  <meta name="author" content="天瓜网|天瓜|每日一美文,胜过十本书" /> 
  <link rel="stylesheet" type="text/css" href="/static/index/css/base.min.css" /> 
  <link rel="stylesheet" type="text/css" href="/static/index/css/com.min.css" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
 </head>
 <body> 
  <!--页面开始--> 
  <div class="main-wraper"> 
   <!--头部开始--> {include file='index/lib/header.tpl'} 
   <!--头部结束--> 
   <!--中部--> 
   <div class="main no-log-main"> 
    <!--中部右边开始--> 
    <div class="g-r-n"> 
     <div class="sub-bar"> 
      <p class="hd get_lianbo_hot">阅读排行</p> 
      <dl class="sub-list"> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3256">谁能坚持超过50秒，我请他吃饭!</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3257">你看了必转走的图片</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3275">天瓜独家首发《2048恋爱脱光版》，不玩你就OUT了</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3258">5分钟看完，50年领悟</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3259">看完泪奔，你有资格抱怨吗</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3283">这篇日志和视频曾经感动了上百万人，今天是个特殊的日子，好久没流泪了</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3276">一份没人敢看的中国地图</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3274">你看了必会有接吻的冲动</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3265">一段视频就是一辈子</a> 
       </dt> 
       <dt class="sub-cp dragon"> 
        <a target="_blank" href="http://www.daygua.com/3273">价值5千万的1堂课（不看白不看）</a> 
       </dt> 
      </dl> 
      <p class="ft"><a href="http://www.daygua.com/top">查看更多</a></p> 
     </div> 
    </div> 
    <!--中部右边结束--> 
    <!--中部左边开始--> 
    <div class="main-content"> 
     <div class="article  song"> 
      <ul class="bd article-list doings_list" data-id="" id="doings-list"> 
       <li class="entry"> 
        <div class="art-desc"> 
         <h3 class="art-t"> <b style="font-size:24px;" "="">关于天瓜</b> </h3> 
         <p> <span class="art-main"> <span style="font-family:'Microsoft YaHei';line-height:2;"><span style="font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;</span><strong><span style="font-size:16px;">天瓜网简介：</span></strong><span style="font-size:14px;">&nbsp;</span></span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;天瓜网是每日精选出一篇美文的最受欢迎的传播正能量的清新网站，文章句句经典感动，是每日必读必上的网站，每日一美文，胜过十本书。</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;分享和传播智慧、正能量、青春励志文章，是我们的宗旨。</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp;&nbsp;</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size:16px;"><strong>网站口号：</strong></span>每日一美文，胜过十本书。&nbsp;</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp;&nbsp;</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp; </span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp;</span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:16px;"><strong>创办初衷：</strong></span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;</span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">您还在苦苦追寻每天能为你加油打气的正能量吗？您还在为找不到能够洗礼心灵的伊甸园而烦恼吗？您还在为茫茫信息海中找不到为您的智慧充电的港湾而苦不堪言吗？</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp; </span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp;</span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">在这里，我们每天为您从上万信息量中精选出最具充满感动，充满正能量，充满智慧的文章，每个人每天可能接触很多信息，但真正能留住感动的有几个，所以，请您每天感动的品一篇。</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp;&nbsp;</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size:16px;"><strong>愿景：</strong></span>每天让大家看到最具正能量的美文。</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"> &nbsp;&nbsp;</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;有任何疑问记得联系我们哦：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=1318616435&amp;site=qq&amp;menu=yes"><img border="0" src="./about_files/pa" alt="给我们留言" title="给我们留言" /></a></span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;或者联系：contact@daygua.com</span><br /> <span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp;或者工作机会联系：job@daygua.com</span><span style="font-family:'Microsoft YaHei';line-height:2;font-size:14px;"></span><br /> </span> </p> 
        </div> </li> 
      </ul> 
     </div> 
    </div> 
    <!--中部左边结束--> 
   </div> 
   <!--中部结束--> 
   <!--底部结束--> {include file='index/lib/footer.tpl'} 
   <!--底部结束--> 
  </div> 
  <!--页面结束--> 
 </body>
</html>